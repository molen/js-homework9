let children = document.querySelectorAll('ul.tabs-content>li');
let activeTab;


document.querySelectorAll('.tabs').forEach((tab) => {
    tab.addEventListener('click', (event) => {
        const listItem = event.target;
        if(activeTab) {
            activeTab.classList.remove('active');
        }

        children.forEach((child) => {
            if (child.dataset.name === listItem.id) {
                child.style.display = 'flex';
                listItem.classList.add('active');
                return;
            }
            activeTab = listItem;
            child.style.display = 'none';
        });
    });
});



